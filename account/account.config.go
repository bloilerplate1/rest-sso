package account

const (
	Admin        string = "ADMIN"
	Professional string = "PRO"
	Subscriber   string = "SUBSCRIBER"
	Core         string = "core"
)

var Roles = []string{"ADMIN", "SUBSCRIBER", "PRO"}
var APIs = []string{Core}
var Permissions = []string{"read:any_account", "read:own_account", "delete:any_account"}

var RolesType = []AuthorizationType{
	{Id: "ADMIN", Name: "Administrator", Typename: "AuthorizationType"},
	{Id: "SUBSCRIBER", Name: "Subscriber", Typename: "AuthorizationType"},
	{Id: "PRO", Name: "Professional", Typename: "AuthorizationType"},
}

var PermissionsType = []AuthorizationType{
	{Id: "read:any_account", Name: "read:any_account", Typename: "AuthorizationType"},
	{Id: "read:own_account", Name: "read:own_account", Typename: "AuthorizationType"},
	{Id: "delete:any_account", Name: "delete:any_account", Typename: "AuthorizationType"},
}

// ADMIN AUTHORIZATIONS ======

var AdminDefaultRoles = AuthorizationType{Id: "ADMIN", Name: "Administrator", Typename: "AuthorizationType"}

var AdminDefaultRolesCore = []AuthorizationType{
	{Id: "ADMIN", Name: "Administrator", Typename: "AuthorizationType"},
	{Id: "SUBSCRIBER", Name: "Subscriber", Typename: "AuthorizationType"},
}

var AdminDefaultPermissionsCore = []AuthorizationType{
	{Id: "read:any_account", Name: "read:any_account", Typename: "AuthorizationType"},
	{Id: "read:own_account", Name: "read:own_account", Typename: "AuthorizationType"},
	{Id: "delete:any_account", Name: "delete:any_account", Typename: "AuthorizationType"},
}

var AdminDefaultAuthorizationCore = &Authorization{
	API:         Core,
	Permissions: AdminDefaultPermissionsCore,
	Roles:       AdminDefaultRolesCore,
	Typename:    "Authorization",
}

func setAdminDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		AdminDefaultAuthorizationCore,
	}

	return authorizations
}

// PRO AUTHORIZATIONS ======

var ProDefaultRoles = AuthorizationType{Id: "ADMIN", Name: "Administrator", Typename: "AuthorizationType"}

var ProDefaultRolesCore = []AuthorizationType{
	{Id: "PRO", Name: "Professional", Typename: "AuthorizationType"},
}

var ProDefaultPermissionsCore = []AuthorizationType{
	{Id: "read:own_account", Name: "read:own_account", Typename: "AuthorizationType"},
}

var ProDefaultAuthorizationCore = &Authorization{
	API:         Core,
	Permissions: ProDefaultPermissionsCore,
	Roles:       ProDefaultRolesCore,
	Typename:    "Authorization",
}

func setProDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		ProDefaultAuthorizationCore,
	}

	return authorizations
}

// SUBSCRIBER AUTHORIZATIONS ======

var SubscriberDefaultRoles = AuthorizationType{Id: "ADMIN", Name: "Administrator", Typename: "AuthorizationType"}

var SubscriberDefaultRolesCore = []AuthorizationType{
	{Id: "PRO", Name: "Professional", Typename: "AuthorizationType"},
}

var SubscriberDefaultPermissionsCore = []AuthorizationType{
	{Id: "read:own_account", Name: "read:own_account", Typename: "AuthorizationType"},
}

var SubscriberDefaultAuthorizationCore = &Authorization{
	API:         Core,
	Permissions: SubscriberDefaultPermissionsCore,
	Roles:       SubscriberDefaultRolesCore,
	Typename:    "Authorization",
}

func setSubscriberDefaultAuthorizations() []*Authorization {
	authorizations := []*Authorization{
		AdminDefaultAuthorizationCore,
	}

	return authorizations
}

// UTILITIES AUTHORIZATIONS ======

func defineDefaultAuthorizations(userCategory string) []*Authorization {
	switch userCategory {
	case Admin:
		return setAdminDefaultAuthorizations()
	case Professional:
		return setProDefaultAuthorizations()
	case Subscriber:
		return setSubscriberDefaultAuthorizations()
	default:
		return []*Authorization{}
	}
}
