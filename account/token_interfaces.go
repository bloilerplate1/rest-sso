package account

import (
	"context"
	"time"
)

// TokenService defines methods the handler layer expects to interact
// with in regards to producing JWTs as string
type ITokenService interface {
	NewPairFromUser(ctx context.Context, u *User, prevTokenID string) (*Session, error)
	ValidateIDToken(tokenString string) (*AccessTokenPayload, error)
}

type ITokenRepository interface {
	NewPairFromUser(ctx context.Context, u *User, prevTokenID string) (*Session, error)
	ValidateIDToken(tokenString string) (*AccessTokenPayload, error)
	ValidateRefreshToken(tokenString string) (*RefreshToken, error)
	RefreshAccessToken(ctx context.Context, u *User) (*AccessToken, error)
	SetRefreshToken(ctx context.Context, userID string, tokenID string, expiresIn time.Duration) error
	DeleteRefreshToken(ctx context.Context, userID string, prevTokenID string) error
}
