package account

import (
	"context"
)

//UserService defines methods the handler layer expects
// any service it interacts with to implement
type IUserService interface {
	Get(ctx context.Context, uid string) (*User, error)
	Signup(ctx context.Context, u *User) (interface{}, *Session, error)
	Signin(ctx context.Context, u *User) (string, *Session, error)
	NewPairFromUser(ctx context.Context, u *User) (*Session, error)
	RefreshAccessToken(ctx context.Context, refreshToken string) (*Session, error)
	BlacklistUser(ctx context.Context, uid string, value bool) bool
	RevokeRefreshToken(ctx context.Context, uid string) bool
	Search(ctx context.Context, pager Pager) (*UserListResponse, error)
	Delete(ctx context.Context, uid string) error
	GetAccountAuthorizaions(uid string) ([]*Authorization, error)
	Update(ctx context.Context, uid string, value *FormUpdate) error
}

// UserRepository defines methods the service layer expects
// any repository it interacts with to implement
type IUserRepository interface {
	FindByID(uid string) (*User, error)
	Create(u *User) (interface{}, error)
	Update(ctx context.Context, uid string, field string, value interface{}) error
	GetList(ctx context.Context, pager Pager) (*UserListResponse, error)
	Delete(ctx context.Context, uid string) error
	UpdateAccount(ctx context.Context, uid string, value *FormUpdate) error
}

// // TokenService defines methods the handler layer expects to interact
// // with in regards to producing JWTs as string
// type ITokenService interface {
// 	NewPairFromUser(ctx context.Context, u *User, prevTokenID string) (*TokenPair, error)
// }
