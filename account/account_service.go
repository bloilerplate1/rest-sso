package account

import (
	"context"
	"fmt"
	"log"

	"vauth/utilities"
	"vauth/utilities/errors_handler"
)

type UserService struct {
	UserRepository  IUserRepository
	TokenRepository ITokenRepository
}

// USConfig will hold repositories that will eventually be injected into this
// this service layer
type UserServiceConfig struct {
	UserRepository  IUserRepository
	TokenRepository ITokenRepository
}

type FormUpdate struct {
	Username string `json:"username" bson:"username"`
}

// NewUserService is a factory function for
// initializing a UserService with its repository layer dependencies
func NewUserService(c *UserServiceConfig) IUserService {
	return &UserService{
		UserRepository:  c.UserRepository,
		TokenRepository: c.TokenRepository,
	}
}

func (s *UserService) Search(ctx context.Context, pager Pager) (*UserListResponse, error) {
	// var users *User
	res, err := s.UserRepository.GetList(ctx, pager)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *UserService) Get(ctx context.Context, uid string) (*User, error) {
	return s.UserRepository.FindByID(uid)
}

func (s *UserService) Signup(ctx context.Context, u *User) (interface{}, *Session, error) {
	pw, err := utilities.HashPassword(u.Password)
	userCategory := "ADMIN"

	if err != nil {
		log.Printf("Unable to signup user for email: %v\n", u.Email)
		return nil, nil, errors_handler.NewInternal("UserService::Signup::FAIL_TO_HASH_PASSWORD")
	}

	u.Password = pw
	u.UID = u.Email
	u.Authorizations = defineDefaultAuthorizations(userCategory)
	tokens, _ := s.TokenRepository.NewPairFromUser(ctx, u, "")
	u.RefreshToken = tokens.RefreshToken
	u.Blacklisted = false
	uid, err := s.UserRepository.Create(u)

	if err != nil {
		return nil, nil, err
	}

	return uid, tokens, nil
}

const (
	ADMIN_USER string = "ADMIN_USER"
)

func (s *UserService) Signin(ctx context.Context, u *User) (string, *Session, error) {
	user, err := s.UserRepository.FindByID(u.Email)
	// Will return NotAuthorized to client to omit details of why
	if err != nil {
		return "", nil, err
	}
	// verify password - we previously created this method
	match, err := utilities.ComparePasswords(user.Password, u.Password)

	if err != nil {
		return "", nil, errors_handler.NewInternal("UserService::Signin::")
	}

	if !match {
		return "", nil, errors_handler.NewInvalidAccessToken()
	}

	tokens, _ := s.TokenRepository.NewPairFromUser(ctx, user, "")

	err = s.UserRepository.Update(ctx, user.UID, "refreshToken", tokens.RefreshToken)

	if err != nil {
		return "", nil, err
	}

	return user.UID, tokens, nil
}

func (s *UserService) Update(ctx context.Context, uid string, form *FormUpdate) error {
	return s.UserRepository.UpdateAccount(ctx, uid, form)
}

func (s *UserService) NewPairFromUser(ctx context.Context, u *User) (*Session, error) {
	return s.TokenRepository.NewPairFromUser(ctx, u, "")
}

func (s *UserService) GetAccountAuthorizaions(uid string) ([]*Authorization, error) {
	account, err := s.UserRepository.FindByID(uid)

	if err != nil {
		return nil, err
	}

	return account.Authorizations, nil

}

func (s *UserService) RefreshAccessToken(ctx context.Context, refreshToken string) (*Session, error) {
	rt, err := s.TokenRepository.ValidateRefreshToken(refreshToken)

	if err != nil {
		fmt.Println("TOKEN_PARSE_ERROR")
		return nil, errors_handler.NewInvalidRefreshToken()
	}

	uid := rt.UID
	user, err := s.UserRepository.FindByID(uid)

	if err != nil || user.RefreshToken != refreshToken {
		fmt.Println("TOKEN_MATCH_ERROR")
		return nil, errors_handler.NewInvalidRefreshToken()
	}

	accessToken, err := s.TokenRepository.RefreshAccessToken(ctx, user)

	if err != nil {
		log.Printf("Error generating idToken for uid: %v. Error: %v\n", uid, err.Error())
		return nil, errors_handler.NewInternal("")
	}

	// err = s.UserRepository.Update(ctx, uid, accessToken.SS)

	// if err != nil {
	// 	return nil, errors_handler.NewInternal()
	// }

	return &Session{
		UID:           uid,
		Authenticated: true,
		AccessToken:   accessToken.SS,
		RefreshToken:  user.RefreshToken,
	}, nil
	//return s.UserRepository.FindByID(uid)
}

func (s *UserService) BlacklistUser(ctx context.Context, uid string, value bool) bool {
	err := s.UserRepository.Update(ctx, uid, "blacklisted", value)

	return err == nil
}

func (s *UserService) Delete(ctx context.Context, uid string) error {
	err := s.UserRepository.Delete(ctx, uid)

	if err != nil {
		log.Panicln("err")
		log.Println(err)
		return errors_handler.NewInternal("UserService::Delete::FAIL_TO_DELETE_USER")
	}

	return nil
}

func (s *UserService) RevokeRefreshToken(ctx context.Context, uid string) bool {
	err := s.UserRepository.Update(ctx, uid, "refreshToken", nil)
	fmt.Println(err)
	return err == nil
}

func (s *UserService) getServiceAuthorizations(ctx context.Context, uid string, service string) (*User, *Authorization, error) {
	user, err := s.UserRepository.FindByID(uid)
	if err != nil {
		return nil, nil, err
	}

	return user, extractAuthorizations(user.Authorizations, service), nil
}

func extractAuthorizations(roles []*Authorization, service string) *Authorization {
	for _, role := range roles {
		// rolesName = append(rolesName, role.Id)
		if role.API == service {
			return role
		}
	}
	return &Authorization{}
}
