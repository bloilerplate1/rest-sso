package account

import (
	"context"
	"crypto/rsa"
	"fmt"
	"log"
	"time"
	"vauth/utilities/errors_handler"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/mongo"
)

type AccessTokenPayload struct {
	API             string   `json:"api"`
	UID             string   `json:"uid"`
	Email           string   `json:"email"`
	Username        string   `json:"username"`
	Roles           []string `json:"roles"`
	Permissions     []string `json:"permissions"`
	AccountProvider string   `json:"provider"`
}

type accessTokenCustomClaims struct {
	Payload *AccessTokenPayload
	jwt.StandardClaims
}

type refreshTokenData struct {
	SS        string
	ID        uuid.UUID
	ExpiresIn time.Duration
	UID       string
}

type refreshTokenCustomClaims struct {
	UID string `json:"uid"`
	jwt.StandardClaims
}

type AccessToken struct {
	SS string `json:"accessToken" bson:"accessToken"`
}

type RefreshToken struct {
	ID  uuid.UUID `json:"-"`
	UID string    `json:"-"`
	SS  string    `json:"refreshToken" bson:"refreshToken"`
}

type TokenPair struct {
	AccessToken
	RefreshToken
}

type TokenRepositoryConfig struct {
	TokenRepository       ITokenRepository
	PrivKey               *rsa.PrivateKey
	PubKey                *rsa.PublicKey
	RefreshSecret         string
	IDExpirationSecs      int64
	RefreshExpirationSecs int64
	MongoClient           *mongo.Client
}

type tokenRepository struct {
	PrivKey               *rsa.PrivateKey
	PubKey                *rsa.PublicKey
	RefreshSecret         string
	IDExpirationSecs      int64
	RefreshExpirationSecs int64
	MongoDB               *mongo.Client
	nameDB                string
	context               context.Context
}

type Session struct {
	UID           string `json:"uid"`
	Authenticated bool   `json:"authenticated"`
	AccessToken   string `json:"accessToken"`
	RefreshToken  string `json:"refreshToken"`
}

// NewTokenRepository is a factory for initializing User Repositories
func NewTokenRepository(c *TokenRepositoryConfig) ITokenRepository {
	return &tokenRepository{
		PrivKey:               c.PrivKey,
		PubKey:                c.PubKey,
		RefreshSecret:         c.RefreshSecret,
		IDExpirationSecs:      c.IDExpirationSecs,
		RefreshExpirationSecs: c.RefreshExpirationSecs,
		MongoDB:               c.MongoClient,
		nameDB:                "vdb",
		context:               context.TODO(),
	}
}

// NewPairFromUser creates fresh id and refresh tokens for the current User
// If a previous token is included, the previous token is removed from
// the tokens repository
func (s *tokenRepository) NewPairFromUser(ctx context.Context, u *User, prevTokenID string) (*Session, error) {
	api, found := ctx.Value("API").(string)

	if !found {
		return nil, errors_handler.NewInternal("tokenRepository::NewPairFromUser::API_MISSING_FROM_CONTEXT")
	}

	accessToken, err := generateAccessToken(u, api, s.PrivKey, s.IDExpirationSecs)

	if err != nil {
		return nil, errors_handler.NewInternal(fmt.Sprintf("Error generating idToken for uid: %v. Error: %v\n", u.UID, err.Error()))
	}

	refreshToken, err := generateRefreshToken(u.UID, s.RefreshSecret, s.RefreshExpirationSecs)

	if err != nil {
		return nil, errors_handler.NewInternal(fmt.Sprintf("Error generating refreshToken for uid: %v. Error: %v\n", u.UID, err.Error()))
	}

	return &Session{
		UID:           u.UID,
		Authenticated: true,
		AccessToken:   accessToken,
		RefreshToken:  refreshToken.SS,
	}, nil
}

func (s *tokenRepository) RefreshAccessToken(ctx context.Context, u *User) (*AccessToken, error) {
	api, found := ctx.Value("API").(string)

	if !found {
		return nil, errors_handler.NewInternal("tokenRepository::RefreshAccessToken::API_NOT_SET_TO_CONTEXT")
	}

	accessToken, err := generateAccessToken(u, api, s.PrivKey, s.IDExpirationSecs)

	if err != nil {
		return nil, errors_handler.NewInternal(fmt.Sprintf("Error generating idToken for uid: %v. Error: %v\n", u.UID, err.Error()))
	}

	return &AccessToken{
		SS: accessToken,
	}, nil
}

// ValidateIDToken validates the id token jwt string
// It returns the user extract from the accessTokenCustomClaims
func (s *tokenRepository) ValidateIDToken(tokenString string) (*AccessTokenPayload, error) {
	claims, err := validateIDToken(tokenString, s.PubKey) // uses public RSA key

	// We'll just return unauthorized error in all instances of failing to verify user
	if err != nil {
		log.Printf("INVALID_ACCESS_TOKEN %v\n", err)
		return nil, errors_handler.NewInvalidAccessToken()
	}

	return claims.Payload, nil
}

// ValidateRefreshToken checks to make sure the JWT provided by a string is valid
// and returns a RefreshToken if valid
func (s *tokenRepository) ValidateRefreshToken(tokenString string) (*RefreshToken, error) {
	// validate actual JWT with string a secret
	claims, err := validateRefreshToken(tokenString, s.RefreshSecret)

	// We'll just return unauthorized error in all instances of failing to verify user
	if err != nil {
		log.Printf("Unable to validate or parse refreshToken for token string: %s\n%v\n", tokenString, err)
		return nil, errors_handler.NewInvalidRefreshToken()
	}

	// Standard claims store ID as a string. I want "model" to be clear our string
	// is a UUID. So we parse claims.Id as UUID
	tokenUUID, err := uuid.Parse(claims.Id)

	if err != nil {
		log.Printf("INVALID_REFRESH_TOKEN %s\n%v\n", claims.Id, err)
		return nil, errors_handler.NewInvalidRefreshToken()
	}

	return &RefreshToken{
		SS:  tokenString,
		ID:  tokenUUID,
		UID: claims.UID,
	}, nil
}

// SetRefreshToken stores a refresh token with an expiry time
func (r *tokenRepository) SetRefreshToken(ctx context.Context, userID string, tokenID string, expiresIn time.Duration) error {
	return nil
}

// DeleteRefreshToken used to delete old  refresh tokens
// Services my access this to revolve tokens
func (r *tokenRepository) DeleteRefreshToken(ctx context.Context, userID string, tokenID string) error {
	return nil
}

func ExtractKeyFromAuthorisationTypes(authTypes []AuthorizationType) []string {
	var keys []string

	for _, auth := range authTypes {
		keys = append(keys, auth.Id)
	}

	return keys
}

func extractRolesName(api string, authorizations []*Authorization) ([]string, []string) {
	for _, authorization := range authorizations {
		if authorization.API == api {
			roles := ExtractKeyFromAuthorisationTypes(authorization.Roles)
			permissions := ExtractKeyFromAuthorisationTypes(authorization.Permissions)
			return roles, permissions
		}
	}
	return []string{}, []string{}
}

// generateIDToken generates an IDToken which is a jwt with myCustomClaims
// Could call this GenerateIDTokenString, but the signature makes this fairly clear
func generateAccessToken(u *User, api string, key *rsa.PrivateKey, exp int64) (string, error) {
	unixTime := time.Now().Unix()
	tokenExp := unixTime + exp // 15 minutes from current time
	roles, permissions := extractRolesName(api, u.Authorizations)
	claims := accessTokenCustomClaims{
		Payload: &AccessTokenPayload{
			API:         api,
			Email:       u.Email,
			UID:         u.UID,
			Username:    u.Username,
			Roles:       roles,
			Permissions: permissions,
		},
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  unixTime,
			ExpiresAt: tokenExp,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	ss, err := token.SignedString(key)

	if err != nil {
		log.Println("Failed to sign id token string")
		return "", err
	}

	return ss, nil
}

// generateRefreshToken creates a refresh token
// The refresh token stores only the User's ID, a string
func generateRefreshToken(userID string, key string, exp int64) (*refreshTokenData, error) {
	currentTime := time.Now()
	tokenExp := currentTime.Add(time.Duration(exp) * time.Second) // 3 days
	tokenID, err := uuid.NewRandom()                              // v4 uuid in the google uuid lib

	if err != nil {
		log.Println("Failed to generate refresh token ID")
		return nil, err
	}

	claims := refreshTokenCustomClaims{
		UID: userID,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  currentTime.Unix(),
			ExpiresAt: tokenExp.Unix(),
			Id:        tokenID.String(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(key))

	if err != nil {
		log.Println("Failed to sign refresh token string")
		return nil, err
	}

	return &refreshTokenData{
		SS:        ss,
		ID:        tokenID,
		ExpiresIn: tokenExp.Sub(currentTime),
	}, nil
}

// validateRefreshToken uses the secret key to validate a refresh token
func validateRefreshToken(tokenString string, key string) (*refreshTokenCustomClaims, error) {
	claims := &refreshTokenCustomClaims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})

	// For now we'll just return the error and handle logging in service level
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, fmt.Errorf("refresh token is invalid")
	}

	claims, ok := token.Claims.(*refreshTokenCustomClaims)

	if !ok {
		return nil, fmt.Errorf("refresh token valid but couldn't parse claims")
	}

	return claims, nil
}

// validateIDToken returns the token's claims if the token is valid
func validateIDToken(tokenString string, key *rsa.PublicKey) (*accessTokenCustomClaims, error) {
	claims := &accessTokenCustomClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	// For now we'll just return the error and handle logging in service level
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, fmt.Errorf("ID token is invalid")
	}

	claims, ok := token.Claims.(*accessTokenCustomClaims)

	if !ok {
		return nil, fmt.Errorf("ID token valid but couldn't parse claims")
	}

	return claims, nil
}
