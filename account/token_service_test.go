package account

// import (
// 	"fmt"
// 	"testing"

// 	"github.com/gin-gonic/gin"
// 	"github.com/google/uuid"
// 	"github.com/jacobsngoodwin/memrizr/account/model"
// 	"github.com/stretchr/testify/mock"
// )

// func TestToken(t *testing.T) {
// 	// Setup
// 	gin.SetMode(gin.TestMode)
// 	t.Run("Success", func(t *testing.T) {
// 		uid, _ := uuid.NewRandom()

// 		mockTokenRepository := new(MockTokenRepository)

// 		// instantiate a common token service to be used by all tests
// 		tokenService := NewTokenService(&TSConfig{
// 			TokenRepository:       mockTokenRepository,
// 			PrivKey:               privKey,
// 			PubKey:                pubKey,
// 			RefreshSecret:         secret,
// 			IDExpirationSecs:      idExp,
// 			RefreshExpirationSecs: refreshExp,
// 		})

// 		u := &model.User{
// 			UID:      uid,
// 			Email:    "bob@bob.com",
// 			Password: "blarghedymcblarghface",
// 		}

// 		// Setup mock call responses in setup before t.Run statements
// 		uidErrorCase, _ := uuid.NewRandom()
// 		uErrorCase := &model.User{
// 			UID:      uidErrorCase,
// 			Email:    "failure@failure.com",
// 			Password: "blarghedymcblarghface",
// 		}
// 		prevID := "a_previous_tokenID"

// 		setSuccessArguments := mock.Arguments{
// 			mock.AnythingOfType("*context.emptyCtx"),
// 			u.UID.String(),
// 			mock.AnythingOfType("string"),
// 			mock.AnythingOfType("time.Duration"),
// 		}

// 		setErrorArguments := mock.Arguments{
// 			mock.AnythingOfType("*context.emptyCtx"),
// 			uErrorCase.UID.String(),
// 			mock.AnythingOfType("string"),
// 			mock.AnythingOfType("time.Duration"),
// 		}

// 		deleteWithPrevIDArguments := mock.Arguments{
// 			mock.AnythingOfType("*context.emptyCtx"),
// 			u.UID.String(),
// 			prevID,
// 		}

// 		// mock call argument/responses
// 		mockTokenRepository.On("SetRefreshToken", setSuccessArguments...).Return(nil)
// 		mockTokenRepository.On("SetRefreshToken", setErrorArguments...).Return(fmt.Errorf("Error setting refresh token"))
// 		mockTokenRepository.On("DeleteRefreshToken", deleteWithPrevIDArguments...).Return(nil)
// 	})
// }
