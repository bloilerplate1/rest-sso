package account

type AuthorizationType struct {
	Id       string `json:"id" bson:"id"`
	Name     string `json:"name" bson:"name"`
	Typename string `json:"__typename" bson:"__typename"`
}
