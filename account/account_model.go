package account

import "time"

type Authorization struct {
	API         string              `json:"api" bson:"api"`
	Roles       []AuthorizationType `json:"roles" bson:"roles"`
	Permissions []AuthorizationType `json:"permissions" bson:"permissions"`
	Typename    string              `json:"__typename" bson:"__typename"`
}

type User struct {
	UID               string           `json:"uid" bson:"_id"`
	Email             string           `json:"email" bson:"email" validate:"email" message:"email is invalid"`
	Password          string           `json:"-" bson:"password"`
	Username          string           `json:"username" bson:"username" validate:"required"`
	AccountProvider   string           `json:"accountProvider" bson:"accountProvider"`
	UpdateAt          time.Time        `json:"UPDATE_AT" bson:"UPDATE_AT"`
	CreateAt          time.Time        `json:"CREATE_AT" bson:"CREATE_AT"`
	RefreshToken      string           `json:"refreshToken" bson:"refreshToken"`
	CoreAuthorization Authorization    `json:"coreAuthorization" bson:"coreAuthorization"`
	Blacklisted       bool             `json:"blacklisted" bson:"blacklisted"`
	Typename          string           `json:"__typename" bson:"__typename"`
	Authorizations    []*Authorization `json:"authorizations" bson:"authorizations"`
}
