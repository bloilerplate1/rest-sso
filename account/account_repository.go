package account

import (
	"context"
	"fmt"
	"log"
	"time"
	"vauth/datasource/mongods"
	"vauth/utilities/errors_handler"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserRepository struct {
	MongoDB *mongo.Client
	nameDB  string
	context context.Context
}

type UserRepositoryCong struct {
	MongoClient *mongo.Client
}

type WriteError struct {
	Code    int
	Index   int
	Message string
}

type MongoErrors struct {
	WriteErrors []WriteError
}

type Pager struct {
	Limit int64
	From  int64
}

type UserListResponse struct {
	Users []*User `json:"items"`
	Total int64   `json:"total"`
}

func (MongoErrors) Error() {
	fmt.Println("edd")
}

func NewUserRepository(c *UserRepositoryCong) IUserRepository {
	err := c.MongoClient.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	return &UserRepository{
		MongoDB: c.MongoClient,
		nameDB:  "vdb",
		context: context.TODO(),
	}
}

func (ar *UserRepository) Create(u *User) (interface{}, error) {
	col := ar.MongoDB.Database(ar.nameDB).Collection("user")
	//defer ar.MongoDB.Disconnect(ar.context)
	now := time.Now()
	u.CreateAt = now
	u.UpdateAt = now
	u.Typename = "Account"
	res, err := col.InsertOne(ar.context, u)

	if err != nil {
		log.Print("ERROR")
		return "NONE", mongods.MongoErrHandle(err, u.UID)
	}
	log.Print("SUUCESS")

	return res.InsertedID, nil
}

func (ar *UserRepository) FindByID(id string) (*User, error) {
	var acc *User

	col := ar.MongoDB.Database(ar.nameDB).Collection("user")

	filter := bson.M{"_id": id}
	err := col.FindOne(context.TODO(), filter).Decode(&acc)

	if err != nil {
		return acc, mongods.MongoErrHandle(err, id)
	}

	return acc, nil
}

func (ur *UserRepository) Update(ctx context.Context, uid string, field string, value interface{}) error {
	col := ur.MongoDB.Database(ur.nameDB).Collection("user")

	update := bson.D{{"$set", bson.D{{field, value}, {"UPDATE_AT", time.Now()}}}}

	_, err := col.UpdateOne(ctx, bson.M{"_id": uid}, update)

	if err != nil {
		return errors_handler.NewInternal("UserRepository:Update:FAIL")
	}
	return nil
}

func (ur *UserRepository) UpdateAccount(ctx context.Context, uid string, value *FormUpdate) error {
	col := ur.MongoDB.Database(ur.nameDB).Collection("user")
	// opts := options.Update().SetUpsert(true)

	update := bson.D{{Key: "$set", Value: value}}
	upsert := true
	after := options.After

	opts := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}

	result := col.FindOneAndUpdate(ctx, bson.M{"_id": uid}, update, &opts)

	if result.Err() != nil {
		return result.Err()
	}
	// _, err = mongods.UpdateResponse(result)

	return nil
}

//GetList retrieve list
func (ur *UserRepository) GetList(ctx context.Context, pager Pager) (*UserListResponse, error) {
	col := ur.MongoDB.Database(ur.nameDB).Collection("user")
	total, err := col.CountDocuments(ctx, bson.D{})

	if err != nil {
		return nil, errors_handler.NewInternal("UserRepository:GetList:FAIL")
	}

	limit := pager.Limit
	from := pager.From
	var cpt int64
	var users []*User
	opt := options.Find()
	opt.SetLimit(limit)
	opt.SetSkip(from)
	cur, err := col.Find(ctx, bson.D{{}}, opt)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	for cur.Next(context.TODO()) {
		cpt++
		var p User
		err := cur.Decode(&p)
		if err != nil {
			log.Println(err)
		}

		users = append(users, &p)
	}

	return &UserListResponse{
		Users: users,
		Total: total,
	}, nil
}

//Delete allow to delete
func (ur *UserRepository) Delete(ctx context.Context, uid string) error {
	col := ur.MongoDB.Database(ur.nameDB).Collection("user")

	res, err := col.DeleteOne(ctx, bson.M{"_id": uid})

	log.Println(res.DeletedCount)
	if err != nil {
		return err
	}
	return nil
}

// func isMongoDupKey(err error) bool {
// 	wce, ok := err.(MongoErrors)
// 	log.Println(err)
// 	if !ok {
// 		return false
// 	}
// 	return wce.Code == 11000 || wce.Code == 11001 || wce.Code == 12582 || wce.Code == 16460 && strings.Contains(wce.Message, " E11000 ")
// }

// func mongoError(errs error) {
// 	strErrors := make([]string, len(errs.Error()))

// 	for i, err := range errs.Error() {
// 		strErrors[i] = err.Error()
// 	}
// }
