package account

import (
	"github.com/stretchr/testify/mock"
)

type MockUserRepository struct {
	mock.Mock
}

// FindByID is mock of AccountRepository FindByID
func (m *MockUserRepository) FindByID(id string) (*User, error) {
	ret := m.Called(id)

	var r0 *User

	if ret.Get(0) != nil {
		r0 = ret.Get(0).(*User)
	}

	var r1 error

	if ret.Get(1) != nil {
		r1 = ret.Get(1).(error)
	}

	return r0, r1
}

func (m *MockUserRepository) Create(u *User) (interface{}, error) {
	ret := m.Called(u)

	var r1 error
	if ret.Get(0) != nil {
		r1 = ret.Get(0).(error)
	}

	return true, r1
}
