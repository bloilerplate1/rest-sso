package account

import (
	"context"
	"fmt"
	"testing"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestGet(t *testing.T) {
	// Setup
	gin.SetMode(gin.TestMode)
	t.Run("Success", func(t *testing.T) {
		uid := "hjsdkfnsdjkfnksd"

		mockUserResp := &User{
			UID:   uid,
			Email: "doudou.leydi.fall@gmail.com",
			Name:  "Bobby Bobson",
		}

		mockUserRepository := new(MockUserRepository)
		us := NewUserService(&UserServiceConfig{
			UserRepository: mockUserRepository,
		})
		mockUserRepository.On("FindByID", uid).Return(mockUserResp, nil)

		ctx := context.TODO()
		u, err := us.Get(ctx, uid)

		assert.NoError(t, err)
		assert.Equal(t, u, mockUserResp)
		mockUserRepository.AssertExpectations(t)
	})

	t.Run("Error", func(t *testing.T) {
		uid := "dsadadasdasdasdasd"

		mockUserRepository := new(MockUserRepository)
		as := NewUserService(&UserServiceConfig{
			UserRepository: mockUserRepository,
		})

		mockUserRepository.On("FindByID", uid).Return(nil, fmt.Errorf("Some error down the call chain"))

		ctx := context.TODO()
		u, err := as.Get(ctx, uid)

		assert.Nil(t, u)
		assert.Error(t, err)
		mockUserRepository.AssertExpectations(t)
	})
	// add two cases here
}

func TestSignup(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		uid := "ddwedwefewwefwe"

		mockUser := &User{
			Email:    "bob@bob.com",
			Password: "howdyhoneighbor!",
		}

		mockUserRepository := new(MockUserRepository)
		us := NewUserService(&UserServiceConfig{
			UserRepository: mockUserRepository,
		})

		// We can use Run method to modify the User when the Create method is called.
		//  We can then chain on a Return method to return no error
		mockUserRepository.
			On("Create", mockUser).
			Run(func(args mock.Arguments) {
				UserArg := args.Get(0).(*User) // arg 0 is context, arg 1 is *User
				UserArg.UID = uid
			}).Return(nil)

		ctx := context.TODO()
		_, err := us.Signup(ctx, mockUser)

		assert.NoError(t, err)

		// assert User now has a UserID
		assert.Equal(t, uid, mockUser.UID)

		mockUserRepository.AssertExpectations(t)
	})

	t.Run("Error", func(t *testing.T) {
		mockUser := &User{
			Email:    "bob@bob.com",
			Password: "howdyhoneighbor!",
		}

		mockUserRepository := new(MockUserRepository)
		us := NewUserService(&UserServiceConfig{
			UserRepository: mockUserRepository,
		})

		mockErr := errors_handler.NewConflict("email", mockUser.Email)

		// We can use Run method to modify the User when the Create method is called.
		//  We can then chain on a Return method to return no error
		mockUserRepository.
			On("Create", mockUser).
			Return(mockErr)

		ctx := context.TODO()
		_, err := us.Signup(ctx, mockUser)

		// assert error is error we response with in mock
		assert.EqualError(t, err, mockErr.Error())

		mockUserRepository.AssertExpectations(t)
	})
}
