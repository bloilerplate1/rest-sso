package errors_handler

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type Type string
type Key string

const (
	Authorization        Type = "AUTHORIZATION"
	Unauthorization      Type = "UNAUTHORIZATION"
	Forbidden            Type = "FORBIDDEN"
	BadRequest           Type = "BADREQUEST"
	Conflict             Type = "CONFLICT"
	Internal             Type = "INTERNAL"
	NotFound             Type = "NOTFOUND"
	PayloadTooLarge      Type = "PAYLOADTOOLARGE"
	UnsupportedMediaType Type = "UNSUPPORTEDMEDIATYPE"
	ServiceUnavailable   Type = "SERVICE_UNAVAILABLE"
	InvalidAccessToken   Type = "INVALID_ACCESS_TOKEN"
	InvalidRefreshToken  Type = "INVALID_REFRESH_TOKEN"
	InvalidCredential    Type = "INVALID_CREDENTIAL"
)

type Error struct {
	Type    Type   `json:"type"`
	Key     string `json:"key"`
	Message string `json:"message"`
}

func (e *Error) Error() string {
	return e.Message
}

func (e *Error) Status() int {
	switch e.Type {
	case Authorization:
		return http.StatusUnauthorized
	case BadRequest:
		return http.StatusBadRequest
	case Conflict:
		return http.StatusConflict
	case Internal:
		return http.StatusInternalServerError
	case NotFound:
		return http.StatusNotFound
	case PayloadTooLarge:
		return http.StatusRequestEntityTooLarge
	case UnsupportedMediaType:
		return http.StatusUnsupportedMediaType
	case ServiceUnavailable:
		return http.StatusServiceUnavailable
	case InvalidAccessToken:
		return http.StatusUnauthorized
	case InvalidRefreshToken:
		return http.StatusUnauthorized
	case InvalidCredential:
		return http.StatusUnauthorized
	default:
		return http.StatusInternalServerError
	}
}

func Status(err error) int {
	var e *Error
	if errors.As(err, &e) {
		return e.Status()
	}
	return http.StatusInternalServerError
}

func NewAuthorization() *Error {
	return &Error{
		Type:    Authorization,
		Message: "",
	}
}

func NewUnauthorization(reason string) *Error {
	return &Error{
		Type:    Unauthorization,
		Message: "",
	}
}

func NewForbidden(reason string) *Error {
	return &Error{
		Type:    Forbidden,
		Message: "",
	}
}

func NewBadRequest(key string, value string) *Error {
	k := fmt.Sprintf("%v:%v", strings.ToUpper(key), strings.ToUpper(value))

	return &Error{
		Type:    BadRequest,
		Key:     k,
		Message: fmt.Sprintf("%v is %v", key, value),
	}
}

func NewConflict(name string, value string) *Error {
	return &Error{
		Type:    Conflict,
		Message: fmt.Sprintf("resource: %v with value: %v already exists", name, value),
	}
}

func NewInternal(reason string) *Error {
	log.Println(reason)
	return &Error{
		Type:    Internal,
		Message: fmt.Sprintf("Internal server error."),
	}
}

func NewNotFound(name string, value string) *Error {
	return &Error{
		Type:    NotFound,
		Message: fmt.Sprintf("resource %v with value %v not found", name, value),
	}
}

func NewPayloadTooLarge(maxBodySize int64, contentLength int64) *Error {
	return &Error{
		Type:    PayloadTooLarge,
		Message: fmt.Sprintf("Max payload size of %v exceeded. Actual payload size %v", maxBodySize, contentLength),
	}
}

func NewUnsupportedMediaType(reason string) *Error {
	return &Error{
		Type:    UnsupportedMediaType,
		Message: reason,
	}
}

func NewServiceUnavailable() *Error {
	return &Error{
		Type:    ServiceUnavailable,
		Message: fmt.Sprintf("Service unavailable or timed out"),
	}
}

func NewValidatorError(reason string) *Error {
	return &Error{
		Type:    BadRequest,
		Message: reason,
	}
}

func NewInvalidAccessToken() *Error {
	return &Error{
		Type:    InvalidAccessToken,
		Message: "",
	}
}

func NewInvalidRefreshToken() *Error {
	return &Error{
		Type:    InvalidRefreshToken,
		Message: "",
	}
}

func NewInvalidCredential() *Error {
	return &Error{
		Type:    InvalidCredential,
		Message: "",
	}
}
