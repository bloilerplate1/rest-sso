package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type signoutReq struct {
	UID string `json:"uid" bson:"uid" binding:"required"`
}

func (h *Handler) Signout(c *gin.Context) {
	var req signoutReq
	// t := tokenReq{}

	if ok := bindData(c, &req); !ok {
		return
	}

	res := h.UserService.RevokeRefreshToken(c, req.UID)

	c.JSON(http.StatusOK, res)
}
