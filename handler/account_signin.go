package handler

import (
	"context"
	"log"
	"net/http"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type signinReq struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required,gte=6,lte=30"`
}

// Signin used to authenticate extant user
func (h *Handler) Signin(c *gin.Context) {
	var req signinReq

	if ok := bindData(c, &req); !ok {
		return
	}

	u := &account.User{
		Email:    req.Email,
		Password: req.Password,
	}

	ctx := context.WithValue(context.Background(), "API", account.Core)

	_, tokens, err := h.UserService.Signin(ctx, u)

	if err != nil {
		log.Printf("Failed to sign in user: %v\n", err.Error())
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	// tokens, err := h.TokenRepository.NewPairFromUser(ctx, u, "")

	// if err != nil {
	// 	log.Printf("Failed to create tokens for user: %v\n", err.Error())

	// 	c.JSON(errors_handler.Status(err), gin.H{
	// 		"error": err,
	// 	})
	// 	return
	// }

	c.JSON(http.StatusOK, tokens)
}
