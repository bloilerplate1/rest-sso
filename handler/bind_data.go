package handler

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

// used to help extract validation errors
type invalidArgument struct {
	Field string `json:"field"`
	Value string `json:"value"`
	Tag   string `json:"tag"`
	Param string `json:"param"`
}

// bindData is helper function, returns false if data is not bound
func bindData(c *gin.Context, req interface{}) bool {
	// Bind incoming json to struct and check for validation errors
	if err := c.ShouldBind(req); err != nil {
		log.Printf("Error binding data: %+v\n", err)

		if errs, ok := err.(validator.ValidationErrors); ok {
			// could probably extract this, it is also in middleware_auth_user
			var invalidArgs []invalidArgument
			log.Printf("Error data: %+v\n", errs)
			for _, err := range errs {
				invalidArgs = append(invalidArgs, invalidArgument{
					err.Field(),
					err.Value().(string),
					err.Tag(),
					err.Param(),
				})
			}

			err := errors_handler.NewBadRequest("INVALID_PARAMS", "Invalid request parameters. See invalidArgs")

			c.JSON(err.Status(), gin.H{
				"error":       err,
				"invalidArgs": invalidArgs,
			})
			return false
		}

		fallBack := errors_handler.NewInternal("bindData:ShouldBind:Failed")

		c.JSON(fallBack.Status(), gin.H{"error": fallBack})
		return false
	}

	return true
}

func BindRequest(c *gin.Context, req interface{}) bool {
	// Bind incoming json to struct and check for validation errors
	if err := c.ShouldBind(req); err != nil {
		log.Printf("Error binding data: %+v\n", err)

		if errs, ok := err.(validator.ValidationErrors); ok {
			// could probably extract this, it is also in middleware_auth_user
			log.Printf("Error data: %+v\n", errs)
			var verr validator.ValidationErrors
			// err := errors_handler.NewBadRequest("INVALID_PARAMS", "Invalid request parameters. See invalidArgs")
			if errors.As(err, &verr) {
				for _, f := range verr {
					err := f.ActualTag()
					rr := errors_handler.NewBadRequest(f.Field(), f.ActualTag())
					c.JSON(rr.Status(), gin.H{"error": rr})
					if f.Param() != "" {
						err = fmt.Sprintf("%s=%s", err, f.Param())
						c.JSON(http.StatusBadRequest, gin.H{"error": err})
					}
				}
				return false
			}

			return false
		}

		fallBack := errors_handler.NewInternal("BindRequest:ShouldBind:Failed")

		c.JSON(fallBack.Status(), gin.H{"error": fallBack})
		return false
	}

	return true
}
