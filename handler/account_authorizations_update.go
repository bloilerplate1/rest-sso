package handler

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type accountauthorizationReq struct {
	UID           string                 `json:"uid"`
	Authorization *account.Authorization `json:"authorization" form:"authorization"`
}

// Signup handler
func (h *Handler) UpdateAccountAuthorization(c *gin.Context) {
	// ctx := context.WithValue(context.Background(), "API", account.Core)

	var req accountauthorizationReq

	if ok := bindData(c, &req); !ok {
		return
	}
	log.Println("validateAuthorization", req)
	if err := h.validateAuthorization(req); err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusCreated, true)
}

func (h *Handler) validateAuthorization(authorization accountauthorizationReq) error {
	log.Println("validateAuthorization")
	log.Println(authorization)
	var err = h.Validator.Struct(authorization)
	log.Println("validateAuthorization", err)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fmt.Println(err.Field()) // by passing alt name to ReportError like below
			fmt.Println(err.Tag())
			return errors_handler.NewBadRequest(fmt.Sprintf("INVALID_%s", strings.ToUpper(err.Field())), fmt.Sprintf("%s is not valid", err.Field()))
		}
	}
	return nil
}
