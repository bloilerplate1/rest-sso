package handler

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type searchRequest struct {
	Pager *account.Pager `json:"pager" binding:"required"`
}

func (h *Handler) SearchUser(c *gin.Context) {
	var req searchRequest

	if ok := BindRequest(c, &req); !ok {
		return
	}

	r := &account.Pager{
		From:  req.Pager.From,
		Limit: req.Pager.From,
	}

	if err := validateSearchRequest(r); err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	res, err := h.UserService.Search(c, *req.Pager)

	if err != nil {
		log.Printf("Failed to sign in user: %v\n", err.Error())
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, res)
}

func validateSearchRequest(pager *account.Pager) error {
	var err = validate.Struct(pager)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fmt.Println(err.Field()) // by passing alt name to ReportError like below
			fmt.Println(err.Tag())
			return errors_handler.NewBadRequest(fmt.Sprintf("INVALID_%s", strings.ToUpper(err.Field())), fmt.Sprintf("%s is not valid", err.Field()))
		}
	}
	return nil
}
