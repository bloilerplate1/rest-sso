package middleware

import (
	"strings"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type authHeader struct {
	AccessToken string `header:"Authorization"`
}

type invalidArgument struct {
	Field string `json:"field"`
	Value string `json:"value"`
	Tag   string `json:"tag"`
	Param string `json:"param"`
}

// AuthUser extracts a user from the Authorization header
// which is of the form "Bearer token"
// It sets the user to the context if the user exists
func AuthUser(s account.ITokenService, roles []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		h := authHeader{}

		// bind Authorization Header to h and check for validation errors
		if err := c.ShouldBindHeader(&h); err != nil {
			if errs, ok := err.(validator.ValidationErrors); ok {
				// we used this type in bind_data to extract desired fields from errs
				// you might consider extracting it
				var invalidArgs []invalidArgument

				for _, err := range errs {
					invalidArgs = append(invalidArgs, invalidArgument{
						err.Field(),
						err.Value().(string),
						err.Tag(),
						err.Param(),
					})
				}

				err := errors_handler.NewBadRequest("INVALID_PARAMS", "INVALID_TOKEN")

				c.JSON(err.Status(), gin.H{
					"error":       err,
					"invalidArgs": invalidArgs,
				})
				c.Abort()
				return
			}

			// otherwise error type is unknown
			err := errors_handler.NewInternal("AuthUser:ShouldBindHeader:Unknown")
			c.JSON(err.Status(), gin.H{
				"error": err,
			})
			c.Abort()
			return
		}

		idTokenHeader := strings.Split(h.AccessToken, "Bearer ")

		if len(idTokenHeader) < 2 {
			err := errors_handler.NewInvalidAccessToken()

			c.JSON(err.Status(), gin.H{
				"error": err,
			})
			c.Abort()
			return
		}

		// validate ID token here
		payload, err := s.ValidateIDToken(idTokenHeader[1])

		if err != nil {
			err := errors_handler.NewInvalidAccessToken()
			c.JSON(err.Status(), gin.H{
				"error": err,
			})
			c.Abort()
			return
		}

		if len(roles) > 0 {
			for _, role := range roles {
				if !contains(payload.Roles, role) {
					err := errors_handler.NewForbidden("INVALID_ROLE")
					c.JSON(err.Status(), gin.H{
						"error": err,
					})
					c.Abort()
					return
				}
			}
		}

		c.Set("user", payload)

		c.Next()
	}
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
