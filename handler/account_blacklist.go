package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type blacklistReq struct {
	UID         string `json:"uid" binding:"required"`
	Blacklisted bool   `json:"blacklisted"`
}

// Image handler
func (h *Handler) BlacklistUser(c *gin.Context) {
	var req blacklistReq

	if ok := bindData(c, &req); !ok {
		return
	}

	res := h.UserService.BlacklistUser(c, req.UID, req.Blacklisted)

	c.JSON(http.StatusOK, res)
}
