package handler

import (
	"fmt"
	"net/http"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type userAuthorizationReq struct {
	UID string `json:"uid" bson:"uid" binding:"required"`
}

func (h *Handler) GetAccountAuthorizaions(c *gin.Context) {
	var req userAuthorizationReq
	// t := tokenReq{}

	if ok := bindData(c, &req); !ok {
		return
	}

	authorizations, err := h.UserService.GetAccountAuthorizaions(req.UID)

	if err != nil {
		fmt.Printf("Failed to sign in user: %v\n", err.Error())
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"total": len(authorizations),
		"items": authorizations,
	})
}
