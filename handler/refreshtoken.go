package handler

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type refreshTokenReq struct {
	RefreshToken string `json:"refreshToken" binding:"required"`
}

// Tokens handler
func (h *Handler) RefreshAccessToken(c *gin.Context) {
	ctx := context.WithValue(context.Background(), "API", account.Core)
	var req refreshTokenReq
	// t := account.RefreshToken{}

	if ok := bindData(c, &req); !ok {
		return
	}

	refreshTokenHeader := strings.Split(req.RefreshToken, " ")

	if len(refreshTokenHeader) < 2 {
		fmt.Println("INVALID_TOKEN_FORMAT")
		err := errors_handler.NewInvalidRefreshToken()

		c.JSON(err.Status(), gin.H{
			"error": err,
		})

		c.Abort()
		return
	}

	session, err := h.UserService.RefreshAccessToken(ctx, refreshTokenHeader[1])

	if err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusCreated, session)

}
