package handler

import (
	"net/http"
	"vauth/account"

	"github.com/gin-gonic/gin"
)

func (h *Handler) GetPermissions(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"total": len(account.PermissionsType),
		"items": account.PermissionsType,
	})
}

func (h *Handler) GetRoles(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"total": len(account.RolesType),
		"items": account.RolesType,
	})
}
