package handler

import (
	"context"
	"net/http"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type accountUpdateReq struct {
	UID  string              `json:"uid" binding:"required"`
	Form *account.FormUpdate `json:"form"`
	// AccountProvider string   `json:"accountProvider" binding:"required,oneof=BASIC GOOGLE FACEBOOK APPLE"`
}

// Signup handler
func (h *Handler) UpdateAccount(c *gin.Context) {
	ctx := context.WithValue(context.Background(), "API", account.Core)

	var req accountUpdateReq

	if ok := bindData(c, &req); !ok {
		return
	}

	// u := &account.User{
	// 	Username: req.Username,
	// 	Password: req.Password,
	// 	// AccountProvider: req.AccountProvider,
	// }

	err := h.UserService.Update(ctx, req.UID, req.Form)

	if err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusCreated, true)
}

// func validateFormUpdate(user *account.User) error {
// 	var err = validate.Struct(user)
// 	if err != nil {
// 		for _, err := range err.(validator.ValidationErrors) {
// 			fmt.Println(err.Field()) // by passing alt name to ReportError like below
// 			fmt.Println(err.Tag())
// 			return errors_handler.NewBadRequest(fmt.Sprintf("INVALID_%s", strings.ToUpper(err.Field())), fmt.Sprintf("%s is not valid", err.Field()))
// 		}
// 	}
// 	return nil
// }
