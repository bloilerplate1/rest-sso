package handler

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"vauth/account"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type coreAuthorizationReq struct {
	Roles []string `json:"roles"`
	// Roles           []string `db:"roles" json:"roles" bson:"roles" binding:"oneof=SUBSCRIBER ADMIN MODERATOR"`
	Permissions []string `json:"permissions"`
}

type signupReq struct {
	Username string `json:"username" bson:"username"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
	// AccountProvider string   `json:"accountProvider" binding:"required,oneof=BASIC GOOGLE FACEBOOK APPLE"`
}

// Signup handler
func (h *Handler) Signup(c *gin.Context) {
	ctx := context.WithValue(context.Background(), "API", account.Core)

	var req signupReq

	if ok := bindData(c, &req); !ok {
		return
	}

	u := &account.User{
		Username: req.Username,
		Email:    req.Email,
		Password: req.Password,
		// AccountProvider: req.AccountProvider,
	}

	if err := validateUser(u); err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	_, tokens, err := h.UserService.Signup(ctx, u)

	if err != nil {
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusCreated, tokens)
}

func RolesValidator(fl validator.FieldLevel) bool {
	_ = fl.Field().Interface().([]string)
	fmt.Println("Comparison date")
	return true
}

func validateUser(user *account.User) error {
	var err = validate.Struct(user)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			fmt.Println(err.Field()) // by passing alt name to ReportError like below
			fmt.Println(err.Tag())
			return errors_handler.NewBadRequest(fmt.Sprintf("INVALID_%s", strings.ToUpper(err.Field())), fmt.Sprintf("%s is not valid", err.Field()))
		}
	}
	return nil
}
