package handler

import (
	"log"
	"net/http"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type deleteuserReq struct {
	UID string `json:"uid" bson:"uid" binding:"required"`
}

func (h *Handler) DeleteUser(c *gin.Context) {
	var req deleteuserReq
	// t := tokenReq{}

	if ok := bindData(c, &req); !ok {
		return
	}

	err := h.UserService.Delete(c, req.UID)

	if err != nil {
		log.Printf("Failed to sign in user: %v\n", err.Error())
		c.JSON(errors_handler.Status(err), gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, true)
}
