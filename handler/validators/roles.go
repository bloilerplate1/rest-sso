package custom_validators

import (
	"vauth/account"

	"github.com/go-playground/validator/v10"
)

func rolesValidator(fl validator.FieldLevel) bool {
	val := fl.Field().Interface().([]string)
	for _, v := range val {
		if !contains(account.Roles, v) {
			return false
		}
	}

	return true
}
