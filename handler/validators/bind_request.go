package custom_validators

import (
	"errors"
	"fmt"
	"log"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func parseError(verr validator.ValidationErrors) map[string]string {
	errs := make(map[string]string)

	for _, f := range verr {
		err := f.ActualTag()
		if f.Param() != "" {
			err = fmt.Sprintf("%s=%s", err, f.Param())
		}
		errs[f.Field()] = err
	}

	log.Println("errs")
	log.Println(errs)

	return errs
}

func BindRequest(c *gin.Context, req interface{}) bool {
	// Bind incoming json to struct and check for validation errors
	if err := c.ShouldBind(req); err != nil {
		log.Printf("Error binding data: %+v\n", err)

		if errs, ok := err.(validator.ValidationErrors); ok {
			// could probably extract this, it is also in middleware_auth_user
			log.Printf("Error data: %+v\n", errs)
			var verr validator.ValidationErrors
			err := errors_handler.NewBadRequest("INVALID_PARAMS", "Invalid request parameters. See invalidArgs")
			if errors.As(err, &verr) {
				c.JSON(err.Status(), gin.H{"errors": parseError(verr)})
				return false
			}

			return false
		}

		fallBack := errors_handler.NewInternal("BindRequest:ShouldBind:Failed")

		c.JSON(fallBack.Status(), gin.H{"error": fallBack})
		return false
	}

	return true
}
