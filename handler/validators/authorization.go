package custom_validators

import (
	"fmt"
	"vauth/account"

	"github.com/go-playground/validator/v10"
)

func authorizationValidator(fl validator.FieldLevel) bool {
	fmt.Println("authorizationValidator LOG")
	val := fl.Field().Interface().([]string)

	for _, v := range val {
		if !contains(account.Permissions, v) {
			return false
		}
	}

	return true
}
