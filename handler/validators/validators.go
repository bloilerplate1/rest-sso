package custom_validators

import "github.com/go-playground/validator/v10"

var validate *validator.Validate

func CustomsValidator() *validator.Validate {
	validate = validator.New()
	_ = validate.RegisterValidation("roles", rolesValidator)
	_ = validate.RegisterValidation("permissions", permissionsValidator)
	_ = validate.RegisterValidation("authorization", authorizationValidator)
	return validate
}
