package handler

import (
	"net/http"
	"strings"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
)

type tokenReq struct {
	AccessToken string `json:"accessToken" bson:"accessToken" binding:"required"`
}

// Image handler
func (h *Handler) GetUserFromToken(c *gin.Context) {
	var req tokenReq
	// t := tokenReq{}

	if ok := bindData(c, &req); !ok {
		return
	}

	accessTokenHeader := strings.Split(req.AccessToken, " ")

	if len(accessTokenHeader) < 2 {
		err := errors_handler.NewInvalidRefreshToken()

		c.JSON(err.Status(), gin.H{
			"error": err,
		})

		c.Abort()
		return
	}

	// validate ID token here
	user, err := h.TokenRepository.ValidateIDToken(accessTokenHeader[1])

	if err != nil {
		err := errors_handler.NewInvalidAccessToken()
		c.JSON(err.Status(), gin.H{
			"error": err,
		})
		c.Abort()
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"user": user,
	})
}
