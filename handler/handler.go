package handler

import (
	"net/http"
	"time"
	"vauth/account"
	"vauth/handler/middleware"
	custom_validators "vauth/handler/validators"
	"vauth/utilities/errors_handler"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type Handler struct {
	UserService     account.IUserService
	TokenRepository account.ITokenRepository
	Validator       *validator.Validate
}

// Config will hold services that will eventually be injected into this
// handler layer on handler initialization
type Config struct {
	R               *gin.Engine
	UserService     account.IUserService
	TokenRepository account.ITokenRepository
	validator       *validator.Validate
	BaseURL         string
	TimeoutDuration time.Duration
}

func NewHandler(c *Config) {
	validate = custom_validators.CustomsValidator()
	// Create an account group
	// Create a handler (which will later have injected services)
	h := &Handler{
		UserService:     c.UserService,
		TokenRepository: c.TokenRepository,
		Validator:       validate,
	} // currently has no properties
	// Create a group, or base url for all routes
	g := c.R.Group(c.BaseURL)

	if gin.Mode() != gin.TestMode {
		g.Use(middleware.Timeout(c.TimeoutDuration, errors_handler.NewServiceUnavailable()))
		g.POST("/signup", h.Signup)
		g.POST("/signin", h.Signin)
		g.POST("/signout", h.Signout)
		g.POST("/refresh-access-token", h.RefreshAccessToken)
		g.POST("/validate-token", h.GetUserFromToken)
		g.PUT("/details", h.Details)
		g.POST("/blacklist-user",
			middleware.AuthUser(h.TokenRepository, []string{"ADMIN"}),
			h.BlacklistUser,
		)
		g.POST("/search-users",
			middleware.AuthUser(h.TokenRepository, []string{"ADMIN"}),
			h.SearchUser,
		)
		g.POST("/finduser-by-id",
			middleware.AuthUser(h.TokenRepository, []string{"ADMIN"}),
			h.FindUserByID,
		)
		g.POST("/user-delete",
			middleware.AuthUser(h.TokenRepository, []string{"ADMIN"}),
			h.DeleteUser,
		)
		g.GET("/permissions",
			h.GetPermissions,
		)
		g.GET("/roles",
			h.GetRoles,
		)
		g.POST("/account-authorizations",
			middleware.AuthUser(h.TokenRepository, []string{"ADMIN"}),
			h.GetAccountAuthorizaions,
		)
		g.POST("/account-update",
			h.UpdateAccount,
		)
		g.POST("/account-authorization-update",
			h.UpdateAccountAuthorization,
		)
	} else {
		g.PUT("/details", h.Details)
		g.GET("/user-roles",
			h.GetAccountAuthorizaions,
		)
	}
}

// Details handler
func (h *Handler) Details(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"hello": "it's details",
	})
}
