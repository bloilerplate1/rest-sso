package mongods

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func getCollection(conn *mongo.Client, databaseName string, collectionName string) (*mongo.Collection, context.Context, context.CancelFunc) {
	c := conn.Database(databaseName).Collection(collectionName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	return c, ctx, cancel
}

//Mongo is the struct for the mongo connection
type Mongo struct {
	conn           *mongo.Client
	dbName         string
	collectionName string
}

//GetRepository return store
func NewMongoRepository(conn *mongo.Client, dbName string, collectionName string) *Mongo {
	return &Mongo{conn: conn, dbName: dbName, collectionName: collectionName}
}

//GetList retrieve list
func (s Mongo) GetList() (*mongo.Cursor, error) {
	c, ctx, _ := getCollection(s.conn, s.dbName, s.collectionName)
	defer s.conn.Disconnect(ctx)
	opt := options.Find()
	//opt.SetLimit(2)
	cur, err := c.Find(ctx, bson.D{{}}, opt)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return cur, nil
}

//Get return entity
func (s Mongo) Get(id uuid.UUID) (*mongo.SingleResult, error) {
	c, ctx, _ := getCollection(s.conn, s.dbName, s.collectionName)
	defer s.conn.Disconnect(ctx)
	filter := bson.M{"id": id}
	d := c.FindOne(context.TODO(), filter)
	return d, nil
}

//Create insert a new document at database
func (s Mongo) Create(d interface{}) (bool, error) {
	c, ctx, _ := getCollection(s.conn, s.dbName, s.collectionName)
	defer s.conn.Disconnect(ctx)
	res, err := c.InsertOne(ctx, d)
	log.Printf("New Item on database ====> %s", res)
	if err != nil {
		return false, err
	}
	return true, nil
}

//Update update a document
func (s Mongo) Update(id uuid.UUID, content bson.D) (bool, error) {
	c, ctx, _ := getCollection(s.conn, s.dbName, s.collectionName)
	res, err := c.UpdateOne(ctx, bson.D{{Key: "id", Value: id}}, content)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	log.Println(res)
	return (res.ModifiedCount > 0), nil
}

//Delete delete document
func (s Mongo) Delete(id uuid.UUID) (bool, error) {
	c, ctx, _ := getCollection(s.conn, s.dbName, s.collectionName)
	res, err := c.DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	return (res.DeletedCount > 0), nil
}
