package mongods

import (
	"fmt"
	"vauth/utilities/errors_handler"

	"go.mongodb.org/mongo-driver/mongo"
)

func MongoErrHandle(err error, options string) *errors_handler.Error {
	fmt.Println(err)
	if mongo.IsDuplicateKeyError(err) {
		return errors_handler.NewConflict("user", options)
	}

	if err == mongo.ErrNoDocuments {
		return errors_handler.NewNotFound("account", options)
	}

	return errors_handler.NewInternal("MongoErrHandle:Fail")
}

func UpdateResponse(result *mongo.UpdateResult) (bool, *errors_handler.Error) {
	if result.MatchedCount == 0 {
		return false, errors_handler.NewNotFound("", "")
	}

	if result.MatchedCount == 1 && result.ModifiedCount == 0 {
		fmt.Println("result.ModifiedCount")
		fmt.Println(result.ModifiedCount)
		fmt.Println("result.UpsertedCount")
		fmt.Println(result.UpsertedCount)
		fmt.Println("result.UpsertedID")
		fmt.Println(result.UpsertedID)
		return false, errors_handler.NewInternal("UpdateResponse:Failed")
	}

	return true, nil
}
