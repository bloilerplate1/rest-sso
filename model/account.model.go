package model

import (
	"github.com/google/uuid"
)

type Account struct {
	UID      uuid.UUID `db:"uid" json:"uid" bson:"_id"`
	Email    string    `db:"email" json:"email" bson:"email"`
	Password string    `db:"password" json:"-" bson:"-"`
	Name     string    `db:"name" json:"name" bson:"name"`
	ImageURL string    `db:"image_url" json:"imageUrl" bson:"imageUrl"`
	Website  string    `db:"website" json:"website" bson:"website"`
}
