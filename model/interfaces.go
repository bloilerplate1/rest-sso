package model

import (
	"context"

	"github.com/google/uuid"
)

//AccountService defines methods the handler layer expects
// any service it interacts with to implement
type AccountService interface {
	Get(ctx context.Context, uid uuid.UUID) (*Account, error)
	Signup(ctx context.Context, u *Account) error
}

// AccountRepository defines methods the service layer expects
// any repository it interacts with to implement
type AccountRepository interface {
	FindByID(ctx context.Context, uid uuid.UUID) (*Account, error)
	Create(ctx context.Context, u *Account) error
}
